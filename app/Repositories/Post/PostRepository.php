<?php
namespace App\Repositories\Post;

use App\Repositories\BaseRepository;
use App\Models\Post;
class PostRepository extends BaseRepository implements PostRepositoryInterface
{
    /**
     * Get Model Post
     */
    public function getModel()
    {
        return Post::class;
    }

    /**
     * Search
     * @param $title
     * @param array $title
     * @return mixed
     */
    public function search($title){
        $result = $this -> model -> where('title', 'LIKE', '%'.$title.'%')->get();
        return $result;
    }
}
