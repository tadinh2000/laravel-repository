<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\StorePostRequest;
use App\Repositories\Post\PostRepositoryInterface;
use App\Http\Resources\PostResource;
use App\Http\Resources\PostCollection;

class PostController extends Controller
{
    protected $postRepository;

    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new PostCollection($this->postRepository->getAll());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        return new PostResource($this->postRepository->create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new PostResource($this->postRepository->find($id));
    }

    // public function search(Request $request) {
    //     return new PostResource($this->postRepository->getAll($request->all()));
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return new PostResource(
            $this->postRepository->update($request->id,$request->all())
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json([
            'status' => true,
            'post' => new PostResource($this->postRepository->delete($id))]);
    }

    /**
     * Search the specified resource in storage.
     *
     * @param string $title
     * @return \Illuminate\Http\Response
     */
    public function search($title){
        $request = $this->postRepository->search($title);
        if (is_null($request)) {
            return response()->json('Data not found', 404);
        }
        return response()->json(PostResource::collection($request));
    }
}
