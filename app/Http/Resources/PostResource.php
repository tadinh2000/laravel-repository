<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'des' => $this->des,
            'detail' => $this->detail,
            'category' => $this->categories,
            'public' => $this->public,
            'data_public' => $this->data_public,
            'position' => array_map('intval', explode(',', $this->position)),
            'thumbs' => $this->thumbs
        ];
    }
}
