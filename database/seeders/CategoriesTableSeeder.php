<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Categories;
use DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categories::truncate();
        DB::table('categories')->insert([
            ['id'=>1, "name"=>"Xã Hội"],
            ['id'=>2, "name"=>"Văn Hóa"],
            ['id'=>3, "name"=>"Nghệ Thuật"],
            ['id'=>4, "name"=>"Khoa Học"],
            ['id'=>5, "name"=>"Tự Nhiên"],
            ['id'=>6, "name"=>"Giải Trí"],
            ['id'=>7, "name"=>"Thể Thao"],
            ['id'=>8, "name"=>"Góc Nhìn"],
            ['id'=>9, "name"=>"Thời Sự"],
            ['id'=>10, "name"=>"Giáo Dục"],
            ['id'=>11, "name"=>"Sức Khỏe"],
            ['id'=>12, "name"=>"Số Hóa"],
            ['id'=>13, "name"=>"Pháp Luật"],
        ]);
    }
}
